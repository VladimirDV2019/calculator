const calculatorMathLine = document.getElementById("calculator-math-line-id");
const calcuatorResult = document.getElementById("calculator-result-id");
const calcuatorActions = document.getElementById("calculator-actions-id");

let firstNumber="";
let operation="";
let secondNumber="";

calcuatorActions.addEventListener('click',function(e){
 const button=e.target;
 const buttonValue=button.textContent;
  if(buttonValue === "C"){
    firstNumber="";
    operation="";
    secondNumber="";
    calcuatorResult.textContent="";
  }else if(button.classList.contains("number")){
    if(operation.length!=0){
      secondNumber+=buttonValue;
    }else{
    firstNumber+=buttonValue;
    }
  }else if(buttonValue === "="){
    firstNumber = +firstNumber;
    secondNumber= +secondNumber;
    if(operation === "*"){
      calcuatorResult.textContent=firstNumber*secondNumber;
    }
    else if(operation === "+"){
       calcuatorResult.textContent=firstNumber+secondNumber;
    }
    else if(operation === "-"){
       calcuatorResult.textContent=firstNumber-secondNumber;
    }
    else if(operation === "/"){
       calcuatorResult.textContent=firstNumber/secondNumber;
    }
    else if(operation === "%"){
       calcuatorResult.textContent=firstNumber%secondNumber;
    }
  }else if(buttonValue === "."){
     if(secondNumber.length!=0 && operation.length!=0 && secondNumber.match(/\./)==null){
      secondNumber+=buttonValue;
    }else if(firstNumber.length!=0 && operation.length==0 && firstNumber.match(/\./)==null){
    firstNumber+=buttonValue;
    }
  }
  else{
    operation=buttonValue;
  }
    
  calculatorMathLine.textContent=`${firstNumber} ${operation} ${secondNumber}`;
});
